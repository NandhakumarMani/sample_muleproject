%dw 2.0
output application/json skipNullOn="everywhere"
---
{
(customer: 
		{
			credit_score: payload.customer.creditScore,
			zip_code:payload.customer.zipCode,
			incentives: payload.customer.incentives map(incentivesItem, indexOfIncentives)->
				(incentivesItem)	
		})if(!isEmpty(payload.customer)),
	dealer:
		{
			partner_id: payload.dealer.partnerId,
			dpid: payload.dealer.dpId
		},
	vehicle:
		{
			vin:  payload.vehicle.vin,
			make: payload.vehicle.make,
			model: payload.vehicle.model,
			year: payload.vehicle.year
		},
	(payment:
		{
			"deal_type": payload.payment.dealType,
			"customer_cash": payload.payment.customerCash,
			"term": payload.payment.term,
			"annual_mileage": payload.payment.annualMileage,
			"no_taxes_or_fees": payload.payment.noTaxesOrFees
		})if(!isEmpty(payload.payment)),
	(trade:
		{
			value: payload.trade.value,
			ownership: payload.trade.ownership,
			payoff: payload.trade.payoff,
			mileage: payload.trade.mileage,
			is_offer: payload.trade.isOffer
			 
		})if(!isEmpty(payload.trade)),		
	(plans: payload.plans map(plansItem, indexOfPlans)->	
		{
			id: plansItem.id,
			tier: plansItem.tier
		})if(!isEmpty(payload.plans))
}