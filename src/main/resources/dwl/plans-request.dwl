%dw 2.0
output application/json skipNullOn="everywhere"
---
{
	"dealer[partnerId]": attributes.queryParams.'dealer[partnerId]',
	"vin": attributes.queryParams.'vin',
	"dealType": attributes.queryParams.'dealType',
	"dealRate": attributes.queryParams.'dealRate',
	"dealTerm": attributes.queryParams.'dealTerm',
	"amountFinanced": attributes.queryParams.'amountFinanced'
}