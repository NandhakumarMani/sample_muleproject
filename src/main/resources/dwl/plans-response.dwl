%dw 2.0
output application/json skipNullOn="everywhere"
---
{
  "plans": payload.plans map(planItem, indexOfPlans)->
  {
       "grades": planItem.grades,
          "dpId": planItem.dpid,
	      "title": planItem.title,
	      "subTitle": planItem.subtitle,
	      "category": planItem.category,
	      "description": planItem.description,
	      "taxable": planItem.taxable,
	      "residualize": planItem.residualize,
	      "vendor": planItem.vendor,
	      "dealTypes": planItem.deal_types,
          "msrp": planItem.msrp,
	      "invoicePrice": planItem.invoice_price,
	      "offer": planItem.offer,
	      "cashFeatureRank": planItem.cash_feature_rank,
	      ("tiers": planItem.tiers map(tiersItem, indexOftiers)->
	      	{
	       		"title": tiersItem.title,
	       		"msrp": tiersItem.msrp,
	       		"invoicePrice": tiersItem.invoice_price,
	       		"offer": tiersItem.offer,
	       		"code": tiersItem.code,
	       		"state": tiersItem.state,
	       		"term": tiersItem.term,
	       		"mileage": tiersItem.mileage
        	})if(!isEmpty(planItem.tiers)),
        	"brochureUrl": planItem.brochure_url,
        	"backendProfit": planItem.backend_profit,
        	"defaultTierIndex": planItem.default_tier_index,
        	"id": planItem.id,
        	"tierIsSelectable": planItem.tier_is_selectable        
     }
}