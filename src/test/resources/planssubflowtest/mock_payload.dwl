{
  "dealer_partner": {
    "dpid": "bmwnyc",
    "partner_id": "65775",
    "name": "BMW of Manhattan",
    "address": "555 W 57th St",
    "city": "New York",
    "state": "NY",
    "zip_code": "10019",
    "phone": "646-557-3700",
    "status": "Live"
  },
  "plans": [
    {
      "grades": [
        "new"
      ],
      "dpid": "bmwnyc",
      "title": "BMW Extended Service Contract",
      "subtitle": "BMW of Manhattan",
      "category": "Vehicle Service Contract",
      "description": "Convenient, affordable way to keep you up to date with your factory recommended service. Avoid disruptions to your personal budget when an unexpected covered repair occurs. Finance the cost of the contract in your monthly payment for even more budget control. Pricing and availability of coverage on request for this vehicle.\n\n",
      "taxable": false,
      "residualize": false,
      "deal_types": [
        "cash",
        "finance",
        "lease"
      ],
      "pricing_properties": {
        "product_id": "439",
        "coverage_full_name": "BMW PLATINUM",
        "coverage_keyword_lists": [],
        "deductible_keyword_lists": [],
        "tier_filters": [
          [
            60,
            75000
          ],
          [
            60,
            100000
          ],
          [
            72,
            75000
          ],
          [
            72,
            100000
          ],
          [
            84,
            75000
          ],
          [
            84,
            100000
          ]
        ]
      },
      "msrp": 0.0,
      "invoice_price": 0.0,
      "offer": 0.0,
      "preselected_instore": false,
      "preselected_online": false,
      "backend_profit": false,
      "tiers": [],
      "default_tier_index": 0,
      "id": 29460,
      "tier_is_selectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpid": "bmwnyc",
      "title": "BMW Ultimate Care+",
      "subtitle": "BMW Financial Services",
      "category": "Pre-Paid Maintenance",
      "description": "Includes wear-and-tear replacement for items like brake pads, wiper blades and clutch – a BMW exclusive.\nContact Dealer for additional term/mileage coverages.\nNot available for vehicles with carbon ceramic brakes.",
      "taxable": false,
      "residualize": false,
      "vendor": "BMW Financial Services",
      "deal_types": [
        "cash",
        "lease",
        "finance"
      ],
      "msrp": 0.0,
      "invoice_price": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "3 year / 36k miles",
          "msrp": 749.0,
          "invoice_price": 0.0,
          "offer": 749.0,
          "code": "336",
          "state": "live",
          "term": 36,
          "mileage": 36000
        },
        {
          "title": "4 year / 50K miles",
          "msrp": 2069.0,
          "invoice_price": 0.0,
          "offer": 2069.0,
          "code": "5100",
          "state": "live",
          "term": 48,
          "mileage": 50000
        }
      ],
      "brochure_url": "https://pictures.dealer.com/kunibmw/f75dfd450a0d02b701a6fb3e09f31373.pdf",
      "default_tier_code": "336",
      "backend_profit": true,
      "default_tier_index": 0,
      "id": 29644,
      "tier_is_selectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpid": "bmwnyc",
      "title": "Tire and Wheel Protection",
      "subtitle": "BMW of Manhattan",
      "category": "Tire and Wheel Protection",
      "description": "Unlimited Tire Replacements due to bubbles, flats, punctures, etc... \$0 deductible\nUnlimited Rim Repair or Replacements if they are bent, cracked, damaged due to road hazards (potholes, etc)... \$0 deductible\nBONUS = 4-Cosmetic Repairs (3yr) due to curb rash or 8-Cosmetic Repairs (5yr) due to curb rash... \$0 deductible\nTire replacements due to normal wear & tear are not covered\nAvailable only for vehicles with an MSRP of \$150k or less\n\n",
      "taxable": false,
      "residualize": false,
      "vendor": "BMW Financial Services",
      "deal_types": [
        "cash",
        "lease",
        "finance"
      ],
      "msrp": 0.0,
      "invoice_price": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "36 months",
          "msrp": 1999.0,
          "invoice_price": 0.0,
          "offer": 1999.0,
          "code": "TWT12",
          "state": "live",
          "term": 36,
          "mileage": null
        },
        {
          "title": "60 months",
          "msrp": 2499.0,
          "invoice_price": 0.0,
          "offer": 2499.0,
          "code": "TWT13",
          "state": "live",
          "term": 60,
          "mileage": null
        }
      ],
      "brochure_url": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/Tirewheel.pdf",
      "default_tier_code": "TWT12",
      "backend_profit": true,
      "default_tier_index": 0,
      "id": 29461,
      "tier_is_selectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpid": "bmwnyc",
      "title": "Paint Protection Wrap (City Package)",
      "subtitle": "BMW of Manhattan",
      "category": "Surface Protection",
      "description": "A clear, protective automotive film which keeps your car looking newer, longer! This film application reduces the amount of chips caused by road debris and protects against cracking, yellowing and fading.\nUpgraded packages also available. Ex: (full hood, mirrors, side skirts)",
      "taxable": false,
      "residualize": false,
      "deal_types": [
        "cash",
        "finance",
        "lease"
      ],
      "msrp": 0.0,
      "invoice_price": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "City Package",
          "msrp": 995.0,
          "invoice_price": 0.0,
          "offer": 995.0,
          "code": "01",
          "state": "live",
          "term": null,
          "mileage": null
        }
      ],
      "preselected_instore": false,
      "preselected_online": false,
      "brochure_url": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/PAINT%20PRO%20BROCHURE.pdf",
      "video_url": "https://drive.google.com/file/d/1f-aBueKZIuZ-esJMtQHO9wGPD3zO77e8/view?usp=drivesdk",
      "backend_profit": false,
      "default_tier_index": 0,
      "id": 29456,
      "tier_is_selectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpid": "bmwnyc",
      "title": "Interior/Exterior Protection",
      "subtitle": "ProTect",
      "category": "Surface Protection",
      "description": "Unlimited Paintless Ding / Dent Removal (4\" less, excludes any non-metal exterior parts of the vehicle)\nProtects Exterior from any fading, tree sap, road salt, insect damage, etc\nPrevents Interior / Exterior Stains (if interior stains can't be wiped off or exterior stains can't be washed off, bring the vehicle in and we'll take care of it)\nUNLIMITED cosmetic RIM repairs (due to curb rash)\nPro-tects against odors caused by bacteria, microbes, mold, mildew. Fungi and algae cargo\n\n ",
      "taxable": true,
      "residualize": false,
      "vendor": "ESRA",
      "deal_types": [
        "cash",
        "finance",
        "lease"
      ],
      "msrp": 0.0,
      "invoice_price": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "3 Years",
          "msrp": 1149.0,
          "invoice_price": 0.0,
          "offer": 1149.0,
          "code": "2",
          "state": "live",
          "term": 36,
          "mileage": null
        },
        {
          "title": "5 Years",
          "msrp": 1499.0,
          "invoice_price": 0.0,
          "offer": 1499.0,
          "code": "4",
          "state": "live",
          "term": 60,
          "mileage": null
        }
      ],
      "preselected_instore": false,
      "preselected_online": false,
      "brochure_url": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/PRO-TECT%20BROCHURE.pdf",
      "default_tier_code": "2",
      "backend_profit": true,
      "default_tier_index": 0,
      "id": 29229,
      "tier_is_selectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpid": "bmwnyc",
      "title": "Crystal Fusion",
      "subtitle": "Crystal",
      "category": "Wear and Tear",
      "description": "HD Glare Reduction & SAFETY Windshield Application (Includes Re-Application for 3-5yrs, depending on vehicle term)\nCrystal Fusion™ is unlike any product available in stores or on-line. Crystal Fusion™ does not contain wax-based chemicals which cause yellowing, streaking and uneven windshield wiper blade wear. The Crystal Fusion™ process molecularly bonds to the glass, creating an ultra-hydrophobic surface. The treated windshield micro-beads water and protects against the elements.",
      "taxable": true,
      "residualize": false,
      "vendor": "ESRA",
      "deal_types": [
        "cash",
        "finance",
        "lease"
      ],
      "msrp": 0.0,
      "invoice_price": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "3 Years",
          "msrp": 499.0,
          "invoice_price": 0.0,
          "offer": 499.0,
          "code": "2",
          "state": "live",
          "term": 36,
          "mileage": null
        },
        {
          "title": "5 Years",
          "msrp": 599.0,
          "invoice_price": 0.0,
          "offer": 599.0,
          "code": "4",
          "state": "live",
          "term": 60,
          "mileage": null
        }
      ],
      "preselected_instore": false,
      "preselected_online": false,
      "brochure_url": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/CRYSTALFUSION-BROCHURE.pdf",
      "video_url": "https://cftproducts.com/how-it-works/",
      "default_tier_code": "2",
      "backend_profit": true,
      "default_tier_index": 0,
      "id": 29227,
      "tier_is_selectable": true
    }
  ]
}