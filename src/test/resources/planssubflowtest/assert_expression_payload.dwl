%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "plans": [
    {
      "grades": [
        "new"
      ],
      "dpId": "bmwnyc",
      "title": "BMW Extended Service Contract",
      "subTitle": "BMW of Manhattan",
      "category": "Vehicle Service Contract",
      "description": "Convenient, affordable way to keep you up to date with your factory recommended service. Avoid disruptions to your personal budget when an unexpected covered repair occurs. Finance the cost of the contract in your monthly payment for even more budget control. Pricing and availability of coverage on request for this vehicle.\n\n",
      "taxable": false,
      "residualize": false,
      "dealTypes": [
        "cash",
        "finance",
        "lease"
      ],
      "msrp": 0.0,
      "invoicePrice": 0.0,
      "offer": 0.0,
      "backendProfit": false,
      "defaultTierIndex": 0,
      "id": 29460,
      "tierIsSelectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpId": "bmwnyc",
      "title": "BMW Ultimate Care+",
      "subTitle": "BMW Financial Services",
      "category": "Pre-Paid Maintenance",
      "description": "Includes wear-and-tear replacement for items like brake pads, wiper blades and clutch – a BMW exclusive.\nContact Dealer for additional term/mileage coverages.\nNot available for vehicles with carbon ceramic brakes.",
      "taxable": false,
      "residualize": false,
      "vendor": "BMW Financial Services",
      "dealTypes": [
        "cash",
        "lease",
        "finance"
      ],
      "msrp": 0.0,
      "invoicePrice": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "3 year / 36k miles",
          "msrp": 749.0,
          "invoicePrice": 0.0,
          "offer": 749.0,
          "code": "336",
          "state": "live",
          "term": 36,
          "mileage": 36000
        },
        {
          "title": "4 year / 50K miles",
          "msrp": 2069.0,
          "invoicePrice": 0.0,
          "offer": 2069.0,
          "code": "5100",
          "state": "live",
          "term": 48,
          "mileage": 50000
        }
      ],
      "brochureUrl": "https://pictures.dealer.com/kunibmw/f75dfd450a0d02b701a6fb3e09f31373.pdf",
      "backendProfit": true,
      "defaultTierIndex": 0,
      "id": 29644,
      "tierIsSelectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpId": "bmwnyc",
      "title": "Tire and Wheel Protection",
      "subTitle": "BMW of Manhattan",
      "category": "Tire and Wheel Protection",
      "description": "Unlimited Tire Replacements due to bubbles, flats, punctures, etc... \$0 deductible\nUnlimited Rim Repair or Replacements if they are bent, cracked, damaged due to road hazards (potholes, etc)... \$0 deductible\nBONUS = 4-Cosmetic Repairs (3yr) due to curb rash or 8-Cosmetic Repairs (5yr) due to curb rash... \$0 deductible\nTire replacements due to normal wear & tear are not covered\nAvailable only for vehicles with an MSRP of \$150k or less\n\n",
      "taxable": false,
      "residualize": false,
      "vendor": "BMW Financial Services",
      "dealTypes": [
        "cash",
        "lease",
        "finance"
      ],
      "msrp": 0.0,
      "invoicePrice": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "36 months",
          "msrp": 1999.0,
          "invoicePrice": 0.0,
          "offer": 1999.0,
          "code": "TWT12",
          "state": "live",
          "term": 36
        },
        {
          "title": "60 months",
          "msrp": 2499.0,
          "invoicePrice": 0.0,
          "offer": 2499.0,
          "code": "TWT13",
          "state": "live",
          "term": 60
        }
      ],
      "brochureUrl": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/Tirewheel.pdf",
      "backendProfit": true,
      "defaultTierIndex": 0,
      "id": 29461,
      "tierIsSelectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpId": "bmwnyc",
      "title": "Paint Protection Wrap (City Package)",
      "subTitle": "BMW of Manhattan",
      "category": "Surface Protection",
      "description": "A clear, protective automotive film which keeps your car looking newer, longer! This film application reduces the amount of chips caused by road debris and protects against cracking, yellowing and fading.\nUpgraded packages also available. Ex: (full hood, mirrors, side skirts)",
      "taxable": false,
      "residualize": false,
      "dealTypes": [
        "cash",
        "finance",
        "lease"
      ],
      "msrp": 0.0,
      "invoicePrice": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "City Package",
          "msrp": 995.0,
          "invoicePrice": 0.0,
          "offer": 995.0,
          "code": "01",
          "state": "live"
        }
      ],
      "brochureUrl": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/PAINT%20PRO%20BROCHURE.pdf",
      "backendProfit": false,
      "defaultTierIndex": 0,
      "id": 29456,
      "tierIsSelectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpId": "bmwnyc",
      "title": "Interior/Exterior Protection",
      "subTitle": "ProTect",
      "category": "Surface Protection",
      "description": "Unlimited Paintless Ding / Dent Removal (4\" less, excludes any non-metal exterior parts of the vehicle)\nProtects Exterior from any fading, tree sap, road salt, insect damage, etc\nPrevents Interior / Exterior Stains (if interior stains can't be wiped off or exterior stains can't be washed off, bring the vehicle in and we'll take care of it)\nUNLIMITED cosmetic RIM repairs (due to curb rash)\nPro-tects against odors caused by bacteria, microbes, mold, mildew. Fungi and algae cargo\n\n ",
      "taxable": true,
      "residualize": false,
      "vendor": "ESRA",
      "dealTypes": [
        "cash",
        "finance",
        "lease"
      ],
      "msrp": 0.0,
      "invoicePrice": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "3 Years",
          "msrp": 1149.0,
          "invoicePrice": 0.0,
          "offer": 1149.0,
          "code": "2",
          "state": "live",
          "term": 36
        },
        {
          "title": "5 Years",
          "msrp": 1499.0,
          "invoicePrice": 0.0,
          "offer": 1499.0,
          "code": "4",
          "state": "live",
          "term": 60
        }
      ],
      "brochureUrl": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/PRO-TECT%20BROCHURE.pdf",
      "backendProfit": true,
      "defaultTierIndex": 0,
      "id": 29229,
      "tierIsSelectable": true
    },
    {
      "grades": [
        "new"
      ],
      "dpId": "bmwnyc",
      "title": "Crystal Fusion",
      "subTitle": "Crystal",
      "category": "Wear and Tear",
      "description": "HD Glare Reduction & SAFETY Windshield Application (Includes Re-Application for 3-5yrs, depending on vehicle term)\nCrystal Fusion™ is unlike any product available in stores or on-line. Crystal Fusion™ does not contain wax-based chemicals which cause yellowing, streaking and uneven windshield wiper blade wear. The Crystal Fusion™ process molecularly bonds to the glass, creating an ultra-hydrophobic surface. The treated windshield micro-beads water and protects against the elements.",
      "taxable": true,
      "residualize": false,
      "vendor": "ESRA",
      "dealTypes": [
        "cash",
        "finance",
        "lease"
      ],
      "msrp": 0.0,
      "invoicePrice": 0.0,
      "offer": 0.0,
      "tiers": [
        {
          "title": "3 Years",
          "msrp": 499.0,
          "invoicePrice": 0.0,
          "offer": 499.0,
          "code": "2",
          "state": "live",
          "term": 36
        },
        {
          "title": "5 Years",
          "msrp": 599.0,
          "invoicePrice": 0.0,
          "offer": 599.0,
          "code": "4",
          "state": "live",
          "term": 60
        }
      ],
      "brochureUrl": "https://d7cutrr07i0mu.cloudfront.net/uploads/BMW/BMW%20of%20Manhattan/CRYSTALFUSION-BROCHURE.pdf",
      "backendProfit": true,
      "defaultTierIndex": 0,
      "id": 29227,
      "tierIsSelectable": true
    }
  ]
})