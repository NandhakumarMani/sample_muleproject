{
  "headers": {
    "authorization": "Basic YWRtaW46YWRtaW4=",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "f102a0c6-91b0-46c9-946b-4355ad1e8bc9",
    "host": "0.0.0.0:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {
    "vin": "WBA73AK06N7J89970",
    "dealType": "cash",
    "dealer[partnerId]": "65775"
  },
  "requestUri": "/api/plans?vin=WBA73AK06N7J89970&dealType=cash&dealer[partnerId]=65775",
  "queryString": "vin=WBA73AK06N7J89970&dealType=cash&dealer[partnerId]=65775",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/plans",
  "listenerPath": "/api/*",
  "relativePath": "/api/plans",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/api/plans?vin=WBA73AK06N7J89970&dealType=cash&dealer[partnerId]=65775",
  "rawRequestPath": "/api/plans",
  "remoteAddress": "/127.0.0.1:63242",
  "requestPath": "/api/plans"
}