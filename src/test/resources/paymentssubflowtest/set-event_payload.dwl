{
  "customer": {
    "creditScore": 750,
    "incentives": [
      "loyalty"
    ],
    "zipCode": "30009"
  },
  "dealer": {
    "partnerId": "65775",
    "dpid": "bmync"
  },
  "vehicle": {
    "vin": "3MW5R7J0XM8C22119",
    "make": "Toyota",
    "model": "Camry",
    "year": 2021
  },
  "payment": {
    "dealType": "cash",
    "customerCash": 2000,
    "term": 36,
    "annualMileage": 1235,
    "noTaxesOrFees": false
  },
  "trade": {
    "value": 5000,
    "ownership": "owned",
    "payoff": 5000,
    "mileage": 98000,
    "isOffer": true
  },
  "plans": [
    {
      "id": 29644,
      "tier": "336"
    }
  ]
}