%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "deals": {
    "cash": {
      "dealType": "cash",
      "terms": {
        "msrp": 49275,
        "price": 49275,
        "basePriceBeforeTaxesAndFees": 49275,
        "basePriceAfterFees": 49569.5,
        "totalAddOns": 749,
        "totalAccessories": 0,
        "totalPlans": 749,
        "totalTaxableAddOns": 0,
        "totalNontaxableAddOns": 749,
        "normalizedTaxableAddOnPrice": 1066,
        "normalizedNonTaxableAddOnPrice": 1000,
        "taxRate": 0.066,
        "secondaryTaxRate": 0,
        "localTaxRate": 0,
        "combinedTaxRate": 0.066,
        "salesTax": 3267.66,
        "primaryTax": 3267.66,
        "secondaryTax": 0,
        "localTax": 0,
        "taxSurcharge": 0,
        "taxAdjustment": 0,
        "defaultTaxBonus": false,
        "fees": {
          "acquisition": 0,
          "doc": 175,
          "filing": 0,
          "handling": 0,
          "inspection": 0,
          "license": 47,
          "misc": 60,
          "outOfState": 0,
          "tire": 12.5
        },
        "feesToTax": [
          "doc",
          "filing",
          "misc"
        ],
        "totalFees": 294.5,
        "dealerCash": 0,
        "totalBonus": 0,
        "nontaxableBonus": 0,
        "taxableBonus": 0,
        "rebate": 0,
        "nontaxableRebate": 0,
        "taxableRebate": 0,
        "totalCoupon": 0,
        "addOnDiscount": 0,
        "plansDiscount": 0,
        "tradeEquity": 0,
        "tradeTaxCredit": 0,
        "dueAtSigning": 53586.16,
        "dueAtSigningBeforeTaxesAndFees": 50024,
        "dueToCustomerAtSigning": 0,
        "totalPurchasePrice": 53586.16,
        "totalPurchasePriceBeforeTaxesAndFees": 50024,
        "totalCost": 53586.16,
        "totalCostOfOwnership": 53586.16,
        "totalContribution": 0,
        "profit": 0
      }
    }
  }
})