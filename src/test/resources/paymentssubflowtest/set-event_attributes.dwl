{
  "headers": {
    "authorization": "Basic YWRtaW46YWRtaW4=",
    "content-type": "application/json",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "0d5a2a3a-906e-4c7e-8131-b91f6279eefe",
    "host": "0.0.0.0:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive",
    "content-length": "675"
  },
  "clientCertificate": null,
  "method": "POST",
  "scheme": "https",
  "queryParams": {},
  "requestUri": "/api/payments",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/payments",
  "listenerPath": "/api/*",
  "relativePath": "/api/payments",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/api/payments",
  "rawRequestPath": "/api/payments",
  "remoteAddress": "/127.0.0.1:64349",
  "requestPath": "/api/payments"
}