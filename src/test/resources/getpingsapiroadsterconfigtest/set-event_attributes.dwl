{
  "headers": {
    "authorization": "Basic YWRtaW46YWRtaW4=",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "e6335baf-c970-4925-9395-4fd31a551f99",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/api/ping",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/ping",
  "listenerPath": "/api/*",
  "relativePath": "/api/ping",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/api/ping",
  "rawRequestPath": "/api/ping",
  "remoteAddress": "/127.0.0.1:50454",
  "requestPath": "/api/ping"
}