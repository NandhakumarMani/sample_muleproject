%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "applicationName": "sapi-roadster",
  "timestamp": "2021-05-07T03:19:01.114Z",
  "environment": "dev",
  "status": "Active"
})