{
  "headers": {
    "authorization": "Basic YWRtaW46YWRtaW4=",
    "user-agent": "PostmanRuntime/7.28.4",
    "accept": "*/*",
    "postman-token": "6bc320c9-97fe-4796-a753-2cd8f1368dd8",
    "host": "localhost:8081",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "http",
  "queryParams": {},
  "requestUri": "/console",
  "queryString": "",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/",
  "listenerPath": "/console/*",
  "relativePath": "/console",
  "localAddress": "/127.0.0.1:8081",
  "uriParams": {},
  "rawRequestUri": "/console",
  "rawRequestPath": "/console",
  "remoteAddress": "/127.0.0.1:51325",
  "requestPath": "/console"
}